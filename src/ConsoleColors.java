public class ConsoleColors {
    // Text Reset
    public static final String RESET = "\033[0m";
    // Regular Colors
    public static final String GREEN = "\033[0;32m";
    public static final String YELLOW = "\033[0;33m";
    public static final String PURPLE = "\033[0;35m";
}