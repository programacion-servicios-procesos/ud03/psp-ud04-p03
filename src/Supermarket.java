public class Supermarket {
    private double totalShoppingAmount;
    private double averageWaitingTime;
    private double totalTime;
    private final int numCustomers;

    public Supermarket(int numCustomers) {
        this.numCustomers = numCustomers;
        this.totalTime = 0;
        this.totalShoppingAmount = 0;
        this.averageWaitingTime = 0;
    }

    // Sumamos al balance total del supermercado
    public synchronized void addAmount(double amount) {
        this.totalShoppingAmount += amount;
    }

    public double getTotalShoppingAmount() {
        return totalShoppingAmount;
    }

    // Calculamos los tiempos de espera
    public synchronized void addWait(double waitTime) {
        totalTime += waitTime;
        averageWaitingTime = totalTime / numCustomers;
    }

    public double getAverageWaitingTime() {
        return averageWaitingTime;
    }
}
