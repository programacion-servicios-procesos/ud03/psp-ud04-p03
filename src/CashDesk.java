import java.util.ArrayList;
import java.util.List;

public class CashDesk implements Runnable {
    private final Supermarket supermarket;
    private final List<Customer> customersQueue;
    private Double cashSales;
    private int paidCustomers;

    public CashDesk(Supermarket supermarket, Double cashSales, int paidCustomers) {
        this.supermarket = supermarket;
        this.cashSales = cashSales;
        this.paidCustomers = paidCustomers;
        this.customersQueue = new ArrayList<>();
    }

    public CashDesk(Supermarket supermarket) {
        this(supermarket, 0.00, 0);
    }

    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();
        for (int i = 0; i < customersQueue.size(); i++) {
            long startCashTime = System.nanoTime();

            // Recuperamos los datos de los clientes
            int sleepPayment = customersQueue.get(i).getSleepPayment();
            double amount = customersQueue.get(i).getShoppingAmount();
            String customerName = customersQueue.get(i).getName();

            // Iniciamos threads de clientes
            Thread customerThread = new Thread(customersQueue.get(i), customerName);
            customerThread.start();

            // Tiempo que tarda en hacer cada compra
            try {
                Thread.sleep(sleepPayment);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            supermarket.addAmount(amount);
            cashSales += amount;
            paidCustomers++;

            // Mensaje de tiempo siendo atendido el cliente
            long endCashTime = System.nanoTime();
            long waitingTime = (endCashTime - startCashTime) / 1000000;
            System.out.println(ConsoleColors.YELLOW + customerName + " pays through " + threadName + ", waiting time: " + waitingTime + "ms" + ConsoleColors.RESET);

            supermarket.addWait(waitingTime);

            // Clientes restantes por atender
            System.out.print("Customers waiting: ");
            for (int j = i + 1; j < customersQueue.size(); j++) {
                System.out.print(ConsoleColors.PURPLE + customersQueue.get(j).getName() + ", " + ConsoleColors.RESET);

                if (j == customersQueue.size()-1) {
                    System.out.println();
                }
            }
        }
        System.out.println("\n" + threadName + " closes.  \t Customer orders: " + paidCustomers + " \t cashier total: " + cashSales + "€");
    }

    public void addQueueCustomerThreads(Customer customer) {
        this.customersQueue.add(customer);
    }
}
