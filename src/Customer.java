public class Customer implements Runnable {
    private final String name;
    private final double shoppingAmount;
    private final int sleepShopping;
    private final int sleepPayment;

    public Customer(String name, double shoppingAmount, int sleepShopping, int sleepPayment) {
        this.name = name;
        this.shoppingAmount = shoppingAmount;
        this.sleepShopping = sleepShopping;
        this.sleepPayment = sleepPayment;
    }

    public double getShoppingAmount() {
        return shoppingAmount;
    }

    public int getSleepPayment() {
        return sleepPayment;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {
        System.out.println(name + " is shopping");
        try {
            Thread.sleep(sleepShopping);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(ConsoleColors.GREEN + name + " finished shopping" + ConsoleColors.RESET);
    }
}
