import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.concurrent.ThreadLocalRandom;

public class MainEx5 {
    private static final int NUM_CUSTOMERS = ThreadLocalRandom.current().nextInt(15, 25);
    private static final int NUM_CASH_DESKS = ThreadLocalRandom.current().nextInt(4, 6);
    private static final CashDesk[] cashDesks = new CashDesk[NUM_CASH_DESKS];
    private static final Customer[] customers = new Customer[NUM_CUSTOMERS];

    private static int sleepPayment() {
        return ThreadLocalRandom.current().nextInt(100, 200);
    }

    private static int sleepShopping() {
        return ThreadLocalRandom.current().nextInt(100, 300);
    }

    private static int randomQueueNumber() {
        return ThreadLocalRandom.current().nextInt(0, NUM_CASH_DESKS);
    }

    public static void main(String[] args) {
        System.out.println("-------------------------------");
        System.out.println(" Total number of cash desks: " + NUM_CASH_DESKS + "\n" + " Total number of customers: " + NUM_CUSTOMERS);
        System.out.println("-------------------------------");
        Supermarket supermarket = new Supermarket(NUM_CUSTOMERS);

        /* Crearmos los threads de las cajas y los clientes */
        for (int i = 0; i < cashDesks.length; i++) {
            cashDesks[i] = new CashDesk(supermarket);
        }
        for (int i = 0; i < customers.length; i++) {
            customers[i] = new Customer("Customer " + (i + 1), shoppingAmount(), sleepShopping(), sleepPayment());
            // Añadir clientes a una caja aleatoria
            cashDesks[randomQueueNumber()].addQueueCustomerThreads(customers[i]);
        }

        // Iniciamos threads de cajas
        Thread[] cashDeskThread = new Thread[NUM_CASH_DESKS];
        for (int i = 0; i < cashDesks.length; i++) {
            cashDeskThread[i] = new Thread(cashDesks[i], "Cash desk " + (i + 1));
            cashDeskThread[i].start();
        }

        // Hacemos que los threads de las cajas esperen
        for (Thread thread : cashDeskThread) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Total balance results
        System.out.println("---------------------------------------------");
        System.out.println(ConsoleColors.YELLOW +  " Total orders amount: " + supermarket.getTotalShoppingAmount() + "€" + ConsoleColors.RESET);
        System.out.println(ConsoleColors.YELLOW +  " Average payment time: " + supermarket.getAverageWaitingTime() + " ms" + ConsoleColors.RESET);
        System.out.println("---------------------------------------------");
    }

    private static double shoppingAmount() {
        DecimalFormatSymbols customSeparators = new DecimalFormatSymbols();
        customSeparators.setDecimalSeparator('.');

        DecimalFormat decimalFormat = new DecimalFormat("##.##", customSeparators);

        return Double.parseDouble(decimalFormat.format(ThreadLocalRandom.current().nextDouble(6, 200)));
    }

}

